

rm(list = ls())

packages <- c("maps","mapproj","mapdata","mapplots","RColorBrewer","randomcoloR","rstudioapi",
              "GGally","corrplot","factoextra","mapplots","dplyr","tidyr","SDMTools","shiny")

## Now load or install & load all
package.check <- lapply(
  packages,
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE)
      library(x, character.only = TRUE)
    }
  }
)


country <- "NLD"   ### chose your country or leave "" to visualise all countries

load(paste0("../Data/FDI_2014_2016",country,".RData"))



server <-shinyServer(function(input, output) {
  
  output$moreControls <- renderUI({ 
      ctry        <-  input$country
      cat(ctry)
      res1        <- subset(dats,  is.element(country, ctry))
      cat(dim(res1))
      tagList( selectInput("gear_type","Gear",unique(res1$gear),multiple=T,selectize = T)  )
      })
  
  output$moreControls2 <- renderUI({ 
    
      gear.type   <-  input$gear_type
      cat(gear.type)
      ctry        <-  input$country
      cat(ctry)
      res1        <- subset(dats, is.element(gear, gear.type)  & is.element(country, ctry))
      cat(dim(res1))
      tagList( selectInput("met5.1","Metier.lev6",unique(res1$fishery),multiple=T,selectize = T)  ,
                sliderInput("lat.range", label = "limits of latitude", min = min(res1$SI_LATI,na.rm=T)-2,
                                                                     max = max(res1$SI_LATI,na.rm=T)+2, value = c(min(res1$SI_LATI,na.rm=T)-2,max(res1$SI_LATI,na.rm=T)+2)) ,
                sliderInput("long.range", label = "limits of longitude", min = min(res1$SI_LONG,na.rm=T)-2,
                                                                     max = max(res1$SI_LONG,na.rm=T)+2, value = c(min(res1$SI_LONG,na.rm=T)-2,max(res1$SI_LONG,na.rm=T)+2)) )
      })



  output$mapMet <- renderPlot({

    latrange   <- input$lat.range
    longrange  <- input$long.range
    yr         <-  input$year.1
    met5   <-  input$met5.1
    Q      <-  as.numeric(input$Q.1)
    gear.type   <-  input$gear_type
    ctry        <-  input$country
    
    
    ##    latrange <- c(48,51) ; longrange <- c(-3,3) ; yr <- 2014:2016 ; met5 <- "OTM_DEF_32-54_0_0" ; Q<- 1:4  ;  cumlim  <- 90
    ##    latrange <- c(48,51) ; longrange <- c(-3,3) ; yr <- 2014:2016 ; met5 <- "NONE" ; Q<- 1:4  ;  cumlim  <- 90 
    
    res1  <- subset(dats , is.element(gear,gear.type)  &  is.element( fishery , met5) & is.element( year , yr) & is.element(quarter , Q) & is.element(country,ctry))
    cat(dim(res1),"\n")
    res2 <- aggregate( landings ~ species + ICES.rectangle + SI_LATI + SI_LONG  , res1 , sum)
    res2  <- tidyr::spread(data = res2 , key = species , value  = landings , fill= 0 )
    res2  <- subset(res2 , SI_LATI>= latrange[1]  & SI_LATI<= latrange[2] )
    res2  <- subset(res2 , SI_LONG>= longrange[1] & SI_LONG<= longrange[2] )
    
    cat(dim(res2),"\n")
    cat(latrange,"\n")
    cat(longrange,"\n")
    
    #names(res2)[3]<- "None"
    tot <- apply(res2[,-c(1:3)],2,sum)
    tot <- round(100*tot/sum(tot),1)
    tot <- sort(tot,decreasing  = T)
    cumlim <- input$cumlim
    cat(cumlim)
    tot[cumsum(tot)>cumlim] <- 0
    tot[tot!=0]<-1
    tot  <- tot[names(res2[,-c(1:3)])]
    Names<- names(tot)
    Names[tot == 0]  <- ""
    
    if(length(Names[Names != ""]) <2)  stop("increase cumulated percentage of total catches")
    
    resmain  <- res2[,  c("SI_LATI","SI_LONG",names(tot)[tot==1])]
    idx <- apply(resmain[,-c(1,2)] , 1, sum)
    resmain  <- resmain[idx>0,]
    pos <- resmain[,1:2]
    resmain <-  resmain[,-c(1:2)]
    if(dim(resmain)[2] == 0)  stop("increase cumulated percentage of total catches")
    resminor <- res2[,  names(tot)[tot==0]]
    print(resmain)
    print(pos)
    ### plot the main metier 7
    coul  <- rainbow(dim(resmain)[2])
    #   
    map(col = "grey80", border = "grey40", fill = TRUE, xlim = c(min(pos[,2])-1,max(pos[,2])+1), ylim =  c(min(pos[,1])-1,max(pos[,1])+1), main = "main metier lev7")
    map(col = "grey80", border = "grey40", fill = TRUE, xlim = longrange, ylim =  latrange, main = "main metier lev7")
    coord2 <- mapproject(pos[,2],pos[,1])#
    for (i in 1:dim(resmain)[1]) add.pie(z=as.matrix(resmain[i,]), x=coord2$x[i], y=coord2$y[i], labels =NA, radius = 0.2 , col= coul)
    add.pie(z=rep(1,dim(resmain)[2]), x=c(min(longrange)+0.95*(max(longrange)-min(longrange))), y=c(min(latrange)+0.10*(max(latrange)-min(latrange))), labels = names(resmain), radius = 0.5 , col= coul)
  })
})






# Define UI for application that draws a histogram
ui<-shinyUI(navbarPage("Task 2 : Relavant fisheries",
                   tabPanel("Maps of landing Sp. comp.",
                            fluidPage(
                              titlePanel(h4("Data selection")),
                              sidebarLayout(position = "left",
                                            sidebarPanel(width= 3 ,
                                              selectInput("country","Country", choice = c(sort(unique(as.character(dats$country)))) ,  multiple=F) ,
                                              uiOutput("moreControls") ,
                                              uiOutput("moreControls2") ,
                                              sliderInput("cumlim", "cumulated percentage of total landings", 50, 100, 95),
                                              checkboxGroupInput("year.1","Year:", choiceNames=c(sort(unique(as.character(dats$year)))) ,
                                                                            choiceValues=c(sort(unique(as.character(dats$year)))) ,
                                                                            selected =(sort(unique(as.character(dats$year)))),
                                                                            inline = T)  ,
                                              checkboxGroupInput("Q.1",label = "Quarter",choices = list("Q1" = 1, "Q2" = 2, "Q3" = 3, "Q4" = 4),selected = 1:4,inline = T)
                                            ),
                                            mainPanel(plotOutput("mapMet", width = '800px', height = '600px')
                                            ))
                            ) #end of FluidPage
                   )#end of tabPanel)
) )


# Run app
shinyApp(ui = ui, server = server)

  
    