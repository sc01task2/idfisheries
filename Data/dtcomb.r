setwd("C:/my git/idfisheries/")
library("vmstools")

d2014<- read.csv("./Data/map__landings_by_rectangle_data2014.csv" , stringsAsFactors=FALSE)
d2015<- read.csv("./Data/map__landings_by_rectangle_data2015.csv" , stringsAsFactors=FALSE)
d2016<- read.csv("./Data/map__landings_by_rectangle_data2016.csv" , stringsAsFactors=FALSE)

countries <- unique(c(d2014$country,d2015$country,d2016$country))


dats <- do.call(rbind, list(d2014,d2015,d2016))
pos<-ICESrectangle2LonLat(dats$ICES.rectangle,midpoint = T)
datsall<-cbind(dats,pos)


for (cou in countries) 
  {
  dats<-subset(datsall,country == cou)
  save(dats , file = paste0("./Data/FDI_2014_2016",cou,".RData"))

}

dats<-datsall
save(dats , file ="./Data/FDI_2014_2016.RData" )
