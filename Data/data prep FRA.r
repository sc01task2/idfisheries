

load("./copy orginial files/FDI_2014_2016FRA.RData")

unique(dats[,c("fishery","gear","mesh.size.range","regulated.gear","vessel.length")])


aggregate(landings~ species,dats,sum)




 aggregate(landings~regulated.gear,dats,sum)



# first separate baltic from north sea   and other regions
dats$region<-"CeltS"
dats$region[is.element(dats$area,c("27.3.C.22", "27.3.D.24", "27.3.D.25", "27.3.D.26", "27.3.D.27", "27.3.D.28", "27.3.D.29"))]  <- "Balt"
dats$region[is.element(dats$area,c("3AN","3AS","4","7D"))]  <- "NorthS"
dats$region[is.element(dats$area,c("8A","8B","8D EU","8D RFMO"))]  <- "BoB"
dats$region[is.element(dats$area,c("2 EU","5A","5B EU","5B COAST","6A","6B EU","6B RFMO","8C","9B EU"))]  <- "Oth"



dats$gear.mesh <- paste(dats$gear,dats$mesh.size.range)
table(dats$mesh.size.range,dats$regulated.gear)
cc<-table(dats$gear.mesh,dats$regulated.gear)
write.table(cc,"table.txt")
cc

library(ggplot2)
ggplot(  aggregate(landings~region,dats,sum) , aes(1,landings,fill = region))+
        geom_bar(width = 1, stat = "identity") +
        coord_polar("y", start=0)



# now merge some metier in the baltic
balt<-subset(dats,region == "Balt")
balt$regulated.gear <- factor(balt$regulated.gear)

table(balt$gear.mesh,balt$regulated.gear)

balt$gear.mesh[balt$regulated.gear == "OTTER"]<- "OTTER <90"
balt$gear.mesh[balt$regulated.gear == "R-OTTER"]<- "OTTER >90"
balt$gear.mesh[balt$regulated.gear == "GILL"]<- "GILL <90"
balt$gear.mesh[balt$regulated.gear == "R-GILL"]<- "GILL >90"
balt$gear.mesh[balt$regulated.gear == "TRAMMEL"]<- "TRAMMEL <90"
balt$gear.mesh[balt$regulated.gear == "R-TRAMMEL"]<- "TRAMMEL >90"

balt$gear.mesh[balt$regulated.gear == "POTS"]<- "POTS"
balt$gear.mesh[balt$regulated.gear == "R-LONGLINE"]<- "LONGLINE"

balt <- subset(balt,!is.element(balt$regulated.gear,c("DREDGE","NONE","R-DEM_SEINE","R-PEL_TRAWL")))


 balt$gear.mesh <- paste("Balt" ,  balt$gear.mesh)


# now merge some metier in the North Sea

NS <-  subset(dats,region == "NorthS")
NS$regulated.gear <- factor(NS$regulated.gear)

table(NS$gear.mesh,NS$regulated.gear)
NS$gear.mesh[NS$regulated.gear == "BT1"]<- "BT1"
#NS$gear.mesh[NS$regulated.gear == "BT2"]<- "BT2"
NS$gear.mesh[NS$gear.mesh == "BEAM 16-31"]<- "TBB_CRU_16-32_0_0"
NS$gear.mesh[NS$gear.mesh == "BEAM 55-69"]<- "TBB_Mussel"
NS$gear.mesh[NS$gear.mesh == "BEAM 70-79"]<- "TBB_Mussel"
NS  <- subset(NS , NS$gear.mesh != "BEAM <16")

NS$gear.mesh[NS$regulated.gear == "TR2"]<- "TR2"
NS$gear.mesh[is.element(NS$gear.mesh,c("OTTER 100-119","DEM_SEINE 100-119"))]<- "TR1 100-119"
NS$gear.mesh[is.element(NS$gear.mesh,c("OTTER >=120","DEM_SEINE >=120"))]<- "TR1 >=120"

NS$gear.mesh[NS$regulated.gear == "POTS"]<- "POTS"
NS$gear.mesh[NS$regulated.gear == "GN1"]<- "GN1"
NS$gear.mesh[NS$regulated.gear == "DREDGE"]<- "DREDGE"
NS <- subset(NS,!is.element(NS$regulated.gear,c("NONE")))

NS$gear.mesh <- paste("NorthS" ,  NS$gear.mesh)
 
 
 
# now merge some metier in the Celtic Sea

cel <-  subset(dats,region == "CeltS")
cel$regulated.gear <- factor(cel$regulated.gear)

ggplot(  aggregate(landings~gear.mesh,cel,sum) , aes(1,landings,fill = gear.mesh))+
        geom_bar(width = 1, stat = "identity") +
        coord_polar("y", start=0)
 
# mostly pelagic 
cel <- cel[grep("PEL_TRAWL",cel$gear.mesh),] 
 
# now merge some metier in the BoB
bob <-  subset(dats,region == "BoB")
bob$regulated.gear <- factor(bob$regulated.gear)

ggplot(  aggregate(landings~gear.mesh,bob,sum) , aes(1,landings,fill = gear.mesh))+
        geom_bar(width = 1, stat = "identity") +
        coord_polar("y", start=0)
 
# mostly pelagic 
bob <- bob[grep("PEL_TRAWL",bob$gear.mesh),] 
 
 
# now merge some metier in the other area

oth <-  subset(dats,region == "Oth")
oth$regulated.gear <- factor(oth$regulated.gear)

ggplot(  aggregate(landings~gear.mesh,oth,sum) , aes(1,landings,fill = gear.mesh))+
        geom_bar(width = 1, stat = "identity") +
        coord_polar("y", start=0)
 
# mostly pelagic 
oth <- oth[grep("PEL_TRAWL",oth$gear.mesh),] 
  
 
 
 
 
#now, recombine the data from the different regions
dats <- do.call (rbind , list(balt,NS,cel,bob,oth))
# remove NorthS in front of PEL_TRAWL so that is get analyse wiht the rest of the pela
#gique data
dats$gear.mesh  <- gsub("NorthS PEL_TRAWL","PEL_TRAWL",dats$gear.mesh )

















 
 


# now merge some of the metiers

dats$fishery <- dats$gear.mesh       



#_______________________________________________________________________________
# now look at the landings per "fishery" to spot if some of them are really small

landz <- aggregate( landings~fishery  , dats, sum)
 pel<-landz
 pel$fishery[grep("PEL",pel$fishery)] <- "PEL"
 pel$fishery[-grep("PEL",pel$fishery)] <- "DEM" 
pel <-aggregate(landings~fishery,pel,sum)

 ggplot(pel , aes(x="",y=landings,fill=fishery)) + geom_bar(width = 1, stat = "identity")+ 
          coord_polar("y", start=0)+
          xlab("")


landz <- aggregate( landings~fishery  , dats[grep("NorthS",dats$fishery),], sum)
landz$Fsave<-landz$fishery
landz$fishery <- gsub("NorthS","",landz$fishery)
#landz <- landz[-grep("PEL",landz$fishery),] 
landz <- landz[rev(order(landz$landings)),]
landz$cum <- cumsum(landz$landings)/sum(landz$landings)
landz$fishery[landz$cum>0.96] <- "other"

 ggplot(landz , aes(x="",y=landings,fill=fishery)) + geom_bar(width = 1, stat = "identity")+ coord_polar("y", start=0)+
    xlab("")  + ggtitle("North Sea")
    
landz <- aggregate( landings~fishery  , dats[grep("Balt",dats$fishery),], sum)
landz$Fsave<-landz$fishery
landz$fishery <- gsub("Balt","",landz$fishery)
landz <- landz[-grep("PEL",landz$fishery),] 
landz <- landz[rev(order(landz$landings)),]
landz$cum <- cumsum(landz$landings)/sum(landz$landings)
landz$fishery[landz$cum>0.96] <- "other"

 ggplot(landz , aes(x="",y=landings,fill=fishery)) + geom_bar(width = 1, stat = "identity")+ coord_polar("y", start=0)+
    xlab("")  + ggtitle("Baltic Sea")
    
    
    


landz <- aggregate( landings~fishery  , dats, sum)
landz$small <- landz$landings<10 # at least 1t per year on average

dats<-merge(dats,landz[,c("fishery","small")] ,all.x=T)


tot <- aggregate(landings~fishery + small,dats,sum)

ggplot(tot[order(tot$landings),],aes(1:dim(tot),landings+5000,fill=small) ) + 
            geom_bar(stat="identity")  +
            scale_fill_discrete(name = "landings < 10") +
            xlab("ad hoc metiers") +
            ylab("total landings 2014-2016\n(tonnes)")
            



# any species that is mainly caught by these small gears?
sp <- aggregate(landings~species+small,dats,sum)
sptot <- aggregate(landings~species,dats,sum)
names(sptot)[2]  <- "landingstot"
sp <-merge(sp,sptot,all.x=T) 
sp <- subset(sp,small == T)
sp$prop <- 100*sp$landings / sp$landingstot         
         
# those small metier land max 5% of a species, except for LUM (lumpsucker)
ggplot(subset(dats ,species == "LUM"),aes(fishery,landings,fill=small)) + geom_bar(stat="identity")        
    # metier PEL_TRAWL 10-199....
ggplot(subset(dats ,fishery == "PEL_TRAWL 100-119"),aes(species,landings,fill=small)) + geom_bar(stat="identity")        
    # ignore it





#dats <- subset(dats , small == F)



### check the issue of the different SPECON

unique(dats$specon)
unique(paste(dats$gear.mesh,dats$specon))

### consider that different SPECON levels should be aggregated
# it makes no sense trying to identify met lev 7 on such a specific activity
dats$specon <- "Not Relevant"
dats2<-aggregate(landings~.,dats,sum)

dim(dats2)
dim(unique(dats[,!is.element(names(dats),c("landings","specon"))]))
  #OK


# remove fresh water fisheries
fresh <- c("FPP","FRO","FPI","FBR","FPE","PLN","FCC")



 dats <- subset(dats2 , !is.element(species,fresh))
save(dats,file ="FDI_2014_2016DEU.RData")

 

landz <- aggregate( landings~fishery , dats, sum)

 
 
 gear<-landz[grep("PEL",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
  
   gear<-landz[grep("BEAM",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
  
   gear<-landz[grep("DEM_SEINE",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
 
 gear<-landz[grep("OTTER",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
 
 
 
  gear<-landz[grep("GILL",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
 


  gear<-landz[grep("TRAMMEL",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
  
  
 
  gear<-landz[grep("DREDGE",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")  
  
  
  
 
  gear<-landz[grep("POT",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")  
  
  






 