

load("./copy orginial files/FDI_2014_2016NLD.RData")

unique(dats[,c("fishery","gear","mesh.size.range","regulated.gear","vessel.length")])

dats$gear.mesh <- paste(dats$gear,dats$mesh.size.range)
table(dats$mesh.size.range,dats$regulated.gear)
table(dats$gear.mesh,dats$regulated.gear)


# now merge some of the metiers
dats$gear.mesh2<-dats$gear.mesh

#BEAM TRAWL
dats$gear.mesh2[is.element(dats$gear.mesh , c("BEAM <16","BEAM 16-31"))]   <- 
                        "BEAM <31"
dats$gear.mesh2[is.element(dats$gear.mesh , c("BEAM 90-99","BEAM 100-119"))]   <- 
                        "BEAM 90-119"

# DEMERSAL SEINE
dats$gear.mesh2[is.element(dats$gear.mesh , c("DEM_SEINE 90-99","DEM_SEINE 100-119"))]   <- 
                        "DEM_SEINE 90-119"

#GILLNETS
with(subset(dats,gear == "GILL") , table(gear.mesh))
# keep them all but some will be ignored in the analyses because their are too small


#OTTER TRAWL
with(subset(dats,gear == "OTTER") , table(gear.mesh))
dats$gear.mesh2[is.element(dats$gear.mesh , c("OTTER 90-99","OTTER 100-119"))]   <- 
                        "OTTER 90-119"
dats$gear.mesh2[is.element(dats$gear.mesh , c("OTTER <16","OTTER 16-31"))]   <- 
                        "OTTER <31"
                        
#PELAGIC TRAWL
with(subset(dats,gear == "PEL_TRAWL") , table(gear.mesh)) 
# keep them all but some will be ignored in the analyses because their are too small

# POTS
with(subset(dats,gear == "POTS") , table(gear.mesh))
# there is one big category which is NONE... I assume with all kinds of meshsize in it,
# keep them separated for the moment and look at landings comp. on the map to see if 
# they correspond to the same fishery, and merge later if relevant

# TRAMMEL NETS
with(subset(dats,gear == "TRAMMEL") , table(gear.mesh))
# keep them all but some will be ignored in the analyses because their are too small



table(dats$gear.mesh2,dats$regulated.gear)

dats$fishery <- dats$gear.mesh2       



#_______________________________________________________________________________
# now look at the landings per "fishery" to spot if some of them are really small

landz <- aggregate( landings~fishery  , dats, sum)
 pel<-landz
 pel$fishery[grep("PEL",pel$fishery)] <- "PEL"
 pel$fishery[-grep("PEL",pel$fishery)] <- "DEM" 
pel <-aggregate(landings~fishery,pel,sum)

 ggplot(pel , aes(x="",y=landings,fill=fishery)) + geom_bar(width = 1, stat = "identity")+ coord_polar("y", start=0)


landz <- aggregate( landings~fishery  , dats, sum)
landz$Fsave<-landz$fishery
landz <- landz[-grep("PEL",landz$fishery),] 
landz <- landz[rev(order(landz$landings)),]
landz$cum <- cumsum(landz$landings)/sum(landz$landings)
landz$fishery[landz$cum>0.96] <- "other"

 ggplot(landz , aes(x="",y=landings,fill=fishery)) + geom_bar(width = 1, stat = "identity")+ coord_polar("y", start=0)

landz <- aggregate( landings~fishery  , dats, sum)
landz$small <- landz$landings<10 # at least 1t per year on average

dats<-merge(dats,landz[,c("fishery","small")] ,all.x=T)


tot <- aggregate(landings~fishery + small,dats,sum)

ggplot(tot[order(tot$landings),],aes(1:dim(tot),landings+5000,fill=small) ) + 
            geom_bar(stat="identity")  +
            scale_fill_discrete(name = "landings < 10") +
            xlab("ad hoc metiers") +
            ylab("total landings 2014-2016\n(tonnes)")
            



# any species that is mainly caught by these small gears?
sp <- aggregate(landings~species+small,dats,sum)
sptot <- aggregate(landings~species,dats,sum)
names(sptot)[2]  <- "landingstot"
sp <-merge(sp,sptot,all.x=T) 
sp <- subset(sp,small == T)
sp$prop <- 100*sp$landings / sp$landingstot         
         
# those small metier land max 5% of a species, except for LUM (lumpsucker)
ggplot(subset(dats ,species == "LUM"),aes(fishery,landings,fill=small)) + geom_bar(stat="identity")        
    # metier PEL_TRAWL 10-199....
ggplot(subset(dats ,fishery == "PEL_TRAWL 100-119"),aes(species,landings,fill=small)) + geom_bar(stat="identity")        
    # ignore it





dats <- subset(dats , small == F)


save(dats,file ="FDI_2014_2016NLD.RData")

lst <- unique(dats[,c("gear.mesh","gear.mesh2","small")])
write.csv(lst , file = "NLD.selectedGears.csv")


 

landz <- aggregate( landings~fishery + small , dats, sum)

 
 
 gear<-landz[grep("PEL",landz$fishery) ,]
 gear<-gear[-grep("SEINE",gear$fishery) ,]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
  
   gear<-landz[grep("BEAM",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
  
   gear<-landz[grep("DEM_SEINE",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")
 
 gear<-landz[grep("OTTER",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
 
 
 
  gear<-landz[grep("GILL",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
 


  gear<-landz[grep("TRAMMEL",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "") 
  
  
 
  gear<-landz[grep("DREDGE",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")  
  
  
  
 
  gear<-landz[grep("POT",landz$fishery),]
 
 ggplot(gear , aes(x=fishery,y=landings,fill=fishery)) + 
 geom_bar(stat = "identity")   +   xlab("") +
 theme(axis.text.x = element_text( angle=90)) +
  scale_fill_discrete(name = "")  
  
  






 