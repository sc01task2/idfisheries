getEflaloMetierLevel7_proby   <- function (dat, analysisName, path, critData = "EURO", runHACinSpeciesSelection = TRUE,
    paramTotal = 95, paramLogevent = 100, critPca = "PCA_70",  algoClust = "CLARA" , testlevel )
    
{

  # dat <- eflalo_x ; path <- subpath   ; critData = "KG"; runHACinSpeciesSelection = TRUE ;  paramTotal = 95; paramLogevent = 100 ; critPca = "PCA_70" ;  algoClust = "CLARA" ; testlevel  =1.96



    # data(correspLevel7to5)
    # data(correspMixedMetier)
    timeStart = Sys.time()
    print("--- CREATING DIRECTORIES AND REDUCING THE EFLALO DATASET TO THE ONLY DATA NECESSARY FOR THE ANALYSIS ---")
    cat("\n")
   # if (!file.exists(analysisName))
#        dir.create(analysisName)
#    setwd(file.path(path, analysisName))
#    if (file.exists(".R_Cache"))
        unlink(".R_Cache", recursive = TRUE)
    eflalo_ori = dat
    #Store(eflalo_ori)
    dat = dat[, c("LE_ID", grep(critData, names(dat), value = TRUE))]
    dat[is.na(dat)] = 0
    null.value <- vector()
    for (i in grep(critData, names(dat))) null.value <- c(null.value,
        which(dat[, i] < 0))
    null.value <- c(null.value, which(apply(dat[, 2:ncol(dat)],
        1, sum, na.rm = TRUE) == 0))
    if (length(null.value) != 0) {
        LogEvent.removed <- dat[sort(unique(null.value)), ]
        dat <- dat[-sort(unique(null.value)), ]
    }
    names(dat)[-1] = unlist(lapply(strsplit(names(dat[, -1]),
        "_"), function(x) x[[3]]))
    dat <- dat[, !names(dat) == "MZZ"]
    #save(dat, file = "dat_cleaned.Rdata")
    
    
    
    print("--- EXPLORING THE DATA FOR SELECTION OF MAIN SPECIES ---")
    cat("\n")
    explo = selectMainSpecies(dat, analysisName, RunHAC = runHACinSpeciesSelection,
        DiagFlag = FALSE)
    Step1 = extractTableMainSpecies(dat, explo$namesMainSpeciesHAC,
        paramTotal = paramTotal, paramLogevent = paramLogevent)
    #save(explo, Step1, file = "Explo_Step1.Rdata")





    rowNamesSave <- row.names(Step1)
    row.names(Step1) <- 1:nrow(Step1)
    if (!file.exists(critPca))
        dir.create(critPca)
    setwd(file.path(path, analysisName, critPca))
    if (file.exists(".R_Cache"))
        unlink(".R_Cache", recursive = TRUE)
    if (critPca == "PCA_70")
        Step2 = getTableAfterPCA(Step1, analysisName, pcaYesNo = "pca",
            criterion = "70percents")
    else if (critPca == "PCA_SC")
        Step2 = getTableAfterPCA(Step1, analysisName, pcaYesNo = "pca",
            criterion = "screetest")
    else if (critPca == "NO_PCA")
        Step2 = getTableAfterPCA(Step1, analysisName, pcaYesNo = "nopca",
            criterion = NULL)
    row.names(Step1) <- rowNamesSave
    row.names(Step2) <- rowNamesSave
    #save(Step1, file = "Step1.Rdata")
    #save(Step2, file = "Step2.Rdata")
    if (!file.exists(algoClust))
        dir.create(algoClust)
    setwd(file.path(path, analysisName, critPca, algoClust))
    if (file.exists(".R_Cache"))
        unlink(".R_Cache", recursive = TRUE)
    if (algoClust == "HAC")
        Step3 = getMetierClusters(Step1, Step2, analysisName,
            methMetier = "hac", param1 = "euclidean", param2 = "ward")
    else if (algoClust == "CLARA")
        Step3 = getMetierClusters_proby(Step1, Step2, analysisName = analysisName,
            methMetier = "clara", param1 = "euclidean", param2 = NULL ,thresholdTestValue = testlevel)
    else if (algoClust == "KMEANS")
        Step3 = getMetierClusters(Step1, Step2, analysisName = analysisName,
            methMetier = "kmeans", param1 = NULL, param2 = NULL)
    #save(Step3, file = "Step3.Rdata")
#    compOrdin = "CompOrdin"
#    if (!file.exists(compOrdin))
#        dir.create(compOrdin)
#    setwd(file.path(path, analysisName, critPca, algoClust, compOrdin))
#    if (file.exists(".R_Cache"))
#        unlink(".R_Cache", recursive = TRUE)
#    if (algoClust == "HAC")
#        clusters = Step3$clusters
#    if (algoClust == "CLARA")
#        clusters = Step3$clusters$clustering
#    if (algoClust == "KMEANS")
#        clusters = Step3$clusters$cluster
#    compMetiers = compareToOrdination(dat = dat, Step2 = Step2,
#        clusters = clusters, targetSpecies = Step3$targetSpecies)
#    #save(compMetiers, file = "compMetiers.Rdata")
   # setwd(file.path(path, analysisName))
    if (!nrow(dat) == nrow(Step3$LE_ID_clust))
        print("--error : number of lines in step 3 not equal to input eflalo, please check!!--")
    dat <- cbind(dat, CLUSTER = Step3$LE_ID_clust[, "clust"])
    if (length(null.value) == 0) {
        eflalo_ori[, "CLUSTER"] = Step3$LE_ID_clust[, "clust"]
    }
    else {
        eflalo_ori[-sort(unique(null.value)), "CLUSTER"] = Step3$LE_ID_clust[,
            "clust"]
    }


    # reduce dimentions by taking only the relevant species
    colsel  <- c(colnames(Step1) ,  "FT_REF","LE_CDATIM", "VE_REF","VE_ID", "VE_FLT","VE_COU","VE_LEN","VE_KW", "VE_TON","FT_DCOU",
                                    "FT_DHAR", "FT_DDAT", "FT_DTIME", "FT_LCOU", "FT_LHAR", "FT_LDAT","FT_LTIME", "LE_ID", "LE_CDAT" ,"LE_STIME",
                                    "LE_ETIME",    "LE_SLAT",  "LE_SLON",   "LE_ELAT",  "LE_ELON",  "LE_GEAR",  "LE_WIDTH", "LE_MSZ","LE_RECT", "LE_DIV",
                                    "LE_SUBDIV",  "LE_MET", "FT_DDATIM", "FT_LDATIM",   "ELID",  "ID",    "LE_MONTH",    "INTV",  "INTVF", "CLUSTER" )

    colsel  <-unique (grep(paste(colsel,collapse="|"), names(eflalo_ori)))
    eflalo_ori <- eflalo_ori[,colsel]
        cat("\n")
    print("Congratulation !! You have now a fully working eflalo dataset with a metier Level 7 !")
    cat("\n")
    print(Sys.time() - timeStart)
    return(eflalo_ori)
}



selectMainSpecies=function(dat,analysisName="",RunHAC=TRUE,DiagFlag=FALSE){
  
  require(FactoMineR)   # function PCA
  require(cluster)      # functions pam & clara
  require(SOAR)         # function Store
  require(amap)         # function hcluster
  require(MASS)         # function lda
  require(mda)          # function fda
  
  p=ncol(dat)   # Number of species
  n=nrow(dat)
  
  # Transform quantities to proportions of total quantity caught by logevent
  print("calculating proportions...") 
  
  propdat=transformation_proportion(dat[,2:p])
  nameSpecies=colnames(propdat)
  nbAllSpecies=ncol(propdat)
  
  t1=Sys.time()           
  
  if (RunHAC == TRUE) {
    
    # METHOD : 'HAC'
    
    print("######## SPECIES EXPLORATION METHOD 1: HAC ########")
    # Transposing data
    table_var=table_variables(propdat)
    
    # HAC
    print("cluster...")
    cah_var=hcluster(table_var, method="euclidean", link="ward")
    
    Store(objects())
    gc(reset=TRUE)
    
    # Select the number of clusters by scree-test
    inerties.vector=cah_var$height[order(cah_var$height,decreasing=TRUE)]
    nb.finalclusters=which(scree(inerties.vector)[,"epsilon"]<0)[1]
    
    if(!is.na(nb.finalclusters)){
      # Dendogram cutting at the selected level
      cah_cluster_var=cutree(cah_var,k=nb.finalclusters)
      
      png(paste(analysisName,"HAC_Dendogram_Step1.png",sep="_"), width = 1200, height = 800)
      plot(cah_var,labels=FALSE,hang=-1,ann=FALSE)
      title(main="HAC dendogram",xlab="Species",ylab="Height")
      rect.hclust(cah_var, k=nb.finalclusters)
      dev.off()
      
      temp=select_species(dat[,2:p],cah_cluster_var)
      namesResidualSpecies=nameSpecies[which(cah_cluster_var==temp[[2]])] #list of residual species
      
      fait=FALSE
      nb_cut=1
      while ((fait == FALSE) && (nb_cut < (p-nb.finalclusters-2))) {
        # cutting below
        print(paste("----------- nb_cut =",nb_cut))
        cah_cluster_var_step=cutree(cah_var,k=(nb.finalclusters+nb_cut))
        # testing residual species
        print(paste("------------- Residual species cluster(s) ",unique(cah_cluster_var_step[namesResidualSpecies])))
        if (length(unique(cah_cluster_var_step[namesResidualSpecies]))==1) {
          print(paste("-------------  No residual cut -----"))
          nb_cut = nb_cut+1 # cutting below
        }else{
          print("-------------  Residual cut -----")
          nbSpeciesClusters=table(cah_cluster_var_step[namesResidualSpecies])
          # testing if a species is alone in a group
          if (sort(nbSpeciesClusters)[1]>1) { # if not alone
            print("------- I stop and have a beer ------")
            fait = TRUE # then I stop
          }else{
            print("------ Updating residual species -----")
            nb_cut = nb_cut+1;  # if alone then cutting below and updating nameResidualSpecies to start again
            numGroupSpeciesAlone = as.numeric(names(sort(nbSpeciesClusters)[1]))
            namesSpeciesAlone = names(cah_cluster_var_step)[which(cah_cluster_var_step==numGroupSpeciesAlone)]
            namesResidualSpecies = namesResidualSpecies[ - which(namesResidualSpecies==namesSpeciesAlone)]
            print(paste("---- Adding new species ---",namesSpeciesAlone))
          }
        }
      } # end of while
      
      
      # If all species are selected step by step, the final k is the initial cut (nb.finalclusters)
      if((nb.finalclusters+nb_cut)>=(p-2)){
        kFinal=nb.finalclusters
        cah_cluster_var=cutree(cah_var,k=kFinal)
        temp=select_species(dat[,2:p],cah_cluster_var)
        namesResidualSpecies=nameSpecies[which(cah_cluster_var==temp[[2]])] #list of residual species
      }
      
      
      # Dendogram of the first cut in the residual species cluster
      png(paste(analysisName,"HAC_Dendogram_Step1_ResidualSpecies.png",sep="_"), width = 1200, height = 800)
      plot(cah_var,labels=FALSE,hang=-1,ann=FALSE)
      title(main="HAC dendogram - Step",xlab="Species",ylab="Height")
      if((nb.finalclusters+nb_cut)>=(p-2)){
        rect.hclust(cah_var, k=kFinal)
      }else{
        rect.hclust(cah_var, k=(nb.finalclusters+nb_cut))
      }
      dev.off()
      
      # Selection of main species
      nomespsel=setdiff(nameSpecies,namesResidualSpecies)
      cat("main species : ",nomespsel,"\n")
      
      # Return the dataset retaining only the main species
      nbMainSpeciesHAC=length(nomespsel)
      namesMainSpeciesHAC=nomespsel
      propNbMainSpeciesHAC=nbMainSpeciesHAC/nbAllSpecies*100
      
      if(DiagFlag==TRUE) {
        datSpeciesWithoutProp=building_tab_pca(dat[,2:p],nomespsel)
        pourcentCatchMainSpeciesHAC=apply(datSpeciesWithoutProp,1,sum)/apply(dat[,2:p],1,sum)*100
        medianPourcentCatchMainSpeciesHAC=median(pourcentCatchMainSpeciesHAC)
      }
      
      Store(objects())
      gc(reset=TRUE)
      
    } else {
      namesMainSpeciesHAC=NA; nbMainSpeciesHAC=as.numeric(NA); medianPourcentCatchMainSpeciesHAC=as.numeric(NA); propNbMainSpeciesHAC=NA
    }
    
    print(Sys.time()-t1)
    
  }else{ namesMainSpeciesHAC=NA; nbMainSpeciesHAC=as.numeric(NA); medianPourcentCatchMainSpeciesHAC=as.numeric(NA); propNbMainSpeciesHAC=NA }
  
  
  # METHOD : 'TOTALE'
  
  print("######## SPECIES EXPLORATION METHOD 2: 'TOTAL' ########")
  
  # Total quantity caught by species
  sumcol=numeric(length=p-1)
  for(i in 2:p){
    sumcol[i-1]=sum(dat[,i], na.rm=TRUE)
  }
  names(sumcol)=names(dat)[-1]
  
  # Percent of each species in the total catch
  propesp=sumcol/sum(sumcol,na.rm=TRUE)*100
  # Columns number of each species by decreasing order of capture
  numesp=order(propesp,decreasing=TRUE)
  # Percent of each species in the total catch by cumulated decreasing order
  propesp=cumsum(propesp[order(propesp,decreasing=TRUE)])
  
  # We are taking all species until having at least seuil% of total catch
  nbMainSpeciesTotal=numeric()
  medianPourcentCatchMainSpeciesTotal=numeric()
  
  for(seuil in seq(5,100,5)){
    cat("seuil:",seuil,"\n")
    pourcent=which(propesp<=seuil)
    # We are taking the name of selected species
    espsel=numesp[1:(length(pourcent)+1)]
    nomespsel=nameSpecies[espsel]
    nbMainSpeciesTotal[seuil/5]=length(nomespsel)
    
    if(DiagFlag==TRUE) {
      # We are building the table with main species and aggregated other species
      datSpeciesWithoutProp=building_tab_pca(dat[,2:p],nomespsel)
      if(length(nomespsel)==1){
        vectorNul=rep(0,n)
        datSpeciesWithoutProp=cbind(datSpeciesWithoutProp,vectorNul)
      }
      pourcentCatchMainSpeciesTotal=apply(datSpeciesWithoutProp,1,sum, na.rm=TRUE)/apply(dat[,2:p],1,sum, na.rm=TRUE)*100
      medianPourcentCatchMainSpeciesTotal[seuil/5]=median(pourcentCatchMainSpeciesTotal)
    }
  }
  nbMainSpeciesTotal=c(0,nbMainSpeciesTotal)
  nbMainSpeciesTotal[length(nbMainSpeciesTotal)]=p-1
  namesMainSpeciesTotal=nomespsel[1:nbMainSpeciesTotal[length(nbMainSpeciesTotal)-1]]
  propNbMainSpeciesTotal=nbMainSpeciesTotal[length(nbMainSpeciesTotal)-1]/nbAllSpecies*100
  
  if (DiagFlag) medianPourcentCatchMainSpeciesTotal=c(0,medianPourcentCatchMainSpeciesTotal)
  
  Store(objects())
  gc(reset=TRUE)
  
  print(Sys.time()-t1)
  
  
  
  # METHOD : 'LOGEVENT'
  
  print("######## SPECIES EXPLORATION METHOD 3: 'LOGEVENT' ########")
  
  nbMainSpeciesLogevent=numeric()
  medianPourcentCatchMainSpeciesLogevent=numeric()
  
  for(seuil in seq(5,100,5)){
    cat("seuil:",seuil,"\n")
    nomespsel=character()
    # We are taking all species with a % of catch >= seuil% for at least one logevent
    for (i in nameSpecies) if (!is.na(any(propdat[,i]>=seuil)) && any(propdat[,i]>=seuil)) nomespsel <- c(nomespsel,i)
    nbMainSpeciesLogevent[seuil/5]=length(nomespsel)
    
    # We are building the table with main species and aggregated other species
    if(DiagFlag==TRUE) {
      datSpeciesWithoutProp=building_tab_pca(dat[,2:p],nomespsel)
      if(length(nomespsel)==1){
        vectorNul=rep(0,n)
        datSpeciesWithoutProp=cbind(datSpeciesWithoutProp,vectorNul)
      }
      pourcentCatchMainSpeciesLogevent=apply(datSpeciesWithoutProp,1,sum)/apply(dat[,2:p],1,sum)*100
      medianPourcentCatchMainSpeciesLogevent[seuil/5]=median(pourcentCatchMainSpeciesLogevent)
    }
  }
  nbMainSpeciesLogevent=c(p-1,nbMainSpeciesLogevent)
  namesMainSpeciesLogevent=nomespsel
  propNbMainSpeciesLogevent=nbMainSpeciesLogevent[length(nbMainSpeciesLogevent)]/nbAllSpecies*100
  
  
  if(DiagFlag) medianPourcentCatchMainSpeciesLogevent=c(100,medianPourcentCatchMainSpeciesLogevent)
  
  print(Sys.time()-t1)
  
  # GRAPHICS
  
  # Number of main species
  X11(5,5)
  plot(seq(0,100,5),nbMainSpeciesTotal,type='l',col="blue",lwd=3, axes=FALSE, xlab="Threshold (%)",ylab="Number of species")
  lines(seq(0,100,5),nbMainSpeciesLogevent,col="green",lwd=3)
  if(!is.na(nbMainSpeciesHAC)) segments(0,nbMainSpeciesHAC,100,nbMainSpeciesHAC,col="red",lwd=3)
  axis(1)
  axis(2, las=2)
  box()
  legend(20, p*0.9, c( "HAC", "PerTotal", "PerLogevent"),lwd=3,col=c("red", "blue", "green"),bty="n")
  savePlot(filename = paste(analysisName,'Number of main species',sep="_"),type ="png")
  dev.off()
  
  X11(5,5)
  plot(seq(0,100,5),nbMainSpeciesTotal,type='l',col="blue",lwd=3, axes=FALSE, xlab="Threshold (%)",ylab="Number of species")
  lines(seq(0,100,5),nbMainSpeciesLogevent,col="green",lwd=3)
  if(!is.na(nbMainSpeciesHAC)) segments(0,nbMainSpeciesHAC,100,nbMainSpeciesHAC,col="red",lwd=3)
  axis(1)
  axis(2, las=2)
  box()
  legend(20, p*0.9, c( "HAC", "PerTotal", "PerLogevent"),lwd=3,col=c("red", "blue", "green"),bty="n")
  savePlot(filename = paste(analysisName,'Number of main species',sep="_"),type ="png")
  dev.off()
  
  # Black and white version
  X11(5,5)
  plot(seq(0,100,5),nbMainSpeciesTotal, type='l' ,lty='dashed', col="black",lwd=3, axes=FALSE, xlab="Threshold (%)",ylab="Number of species")
  lines(seq(0,100,5),nbMainSpeciesLogevent, type='l', lty='dotted', col="black",lwd=3)
  if(!is.na(nbMainSpeciesHAC)) segments(0,nbMainSpeciesHAC,100,nbMainSpeciesHAC,col="black",lwd=3)
  axis(1)
  axis(2, las=2)
  box()
  legend(20, p*0.9, c( "HAC", "PerTotal", "PerLogevent"),lwd=3,col=c("black", "black", "black"),bty="n",lty=c('solid','dashed','dotted'),box.lty = par("lty"))
  savePlot(filename = paste(analysisName,'Number of main species_new_2',sep="_"),type ="png")
  dev.off()
  
  # Median percentage of catch represented by main species by logevent
  if(DiagFlag){
    png(paste(analysisName,"Median percentage of catch represented by main species by logevent.png",sep="_"), width = 1200, height = 800)
    plot(seq(0,100,5),medianPourcentCatchMainSpeciesTotal,type='l',col="blue",lwd=2, main="Median percentage of catch represented by main species by logevent depending of the threshold", xlab="Threshold (%)",ylab="Median percentage of catch represented by main species by logevent")
    lines(seq(0,100,5),medianPourcentCatchMainSpeciesLogevent,col="green",lwd=2)
    if (RunHAC==TRUE) abline(medianPourcentCatchMainSpeciesHAC,0, col="red",lwd=2)
    mtext(paste(p-1," Species"),col='darkblue')
    if (RunHAC==TRUE) legend(70, 40, c("HAC", "Total", "Logevent"),lwd=2,col=c("red", "blue", "green"))
    if (RunHAC==FALSE) legend(70, 40, c("Total", "Logevent"),lwd=2,col=c("blue", "green"))
    dev.off()
  }
  
  listSpecies=sort(unique(c(namesMainSpeciesHAC,namesMainSpeciesTotal,namesMainSpeciesLogevent)))
  
  # Proportion of the total catch represented by the species in listSpecies (= namesMainSpeciesAll)
  catchListSpecies=sumcol[listSpecies]
  propCatchListSpecies=sum(catchListSpecies)/sum(sumcol)*100
  
  
  if(DiagFlag==FALSE) { 
    explo_species = list(nbAllSpecies=nbAllSpecies,
                         propNbMainSpeciesHAC=propNbMainSpeciesHAC,
                         propNbMainSpeciesTotal=propNbMainSpeciesTotal,
                         propNbMainSpeciesLogevent=propNbMainSpeciesLogevent,
                         nbMainSpeciesHAC=nbMainSpeciesHAC, 
                         nbMainSpeciesTotal=nbMainSpeciesTotal, 
                         nbMainSpeciesLogevent=nbMainSpeciesLogevent,
                         namesMainSpeciesHAC=sort(namesMainSpeciesHAC), 
                         namesMainSpeciesTotalAlphabetical=sort(namesMainSpeciesTotal),                                             
                         namesMainSpeciesTotalByImportance=namesMainSpeciesTotal,
                         namesMainSpeciesLogevent=sort(namesMainSpeciesLogevent),
                         namesMainSpeciesAll=listSpecies,
                         propCatchMainSpeciesAll=propCatchListSpecies) 
  }else{         
    explo_species = list(nbAllSpecies=nbAllSpecies,
                         propNbMainSpeciesHAC=propNbMainSpeciesHAC,
                         propNbMainSpeciesTotal=propNbMainSpeciesTotal,
                         propNbMainSpeciesLogevent=propNbMainSpeciesLogevent,
                         nbMainSpeciesHAC=nbMainSpeciesHAC, 
                         nbMainSpeciesTotal=nbMainSpeciesTotal, 
                         nbMainSpeciesLogevent=nbMainSpeciesLogevent,
                         namesMainSpeciesHAC=sort(namesMainSpeciesHAC), 
                         namesMainSpeciesTotalAlphabetical=sort(namesMainSpeciesTotal),                                             
                         namesMainSpeciesTotalByImportance=namesMainSpeciesTotal,
                         namesMainSpeciesLogevent=sort(namesMainSpeciesLogevent),
                         namesMainSpeciesAll=listSpecies,
                         medianPourcentCatchMainSpeciesHAC=median(pourcentCatchMainSpeciesHAC),
                         medianPourcentCatchMainSpeciesTotal=medianPourcentCatchMainSpeciesTotal,
                         medianPourcentCatchMainSpeciesLogevent=medianPourcentCatchMainSpeciesLogevent,
                         propCatchMainSpeciesAll=propCatchListSpecies)
  }    
  
  return(explo_species)
  
}

transformation_proportion=function(tab){
  res=as.matrix(tab)
  n=nrow(tab)
  p=ncol(tab)
  for (i in 1:n){
    sommeligne=sum(res[i,], na.rm=TRUE)
    if(sommeligne==0){
      res[i,]=rep(0,p)
    }else{
      res[i,]=res[i,]*(100/sommeligne)
    }
  }
  return(res)
}

table_variables=function(data){
  n=nrow(data)
  res1=t(as.matrix(data[1:round(n/2),]))
  res2=t(as.matrix(data[(round(n/2)+1):n,]))
  res=cbind(res1,res2)
  row.names(res)=colnames(data)
  colnames(res)=row.names(data)
  return(res)
}

scree=function(eig){
  n=length(eig)
  delta=numeric(n)
  epsilon=numeric(n)
  delta[2]=eig[2]-eig[1]
  for (i in 3:n){
    delta[i]=eig[i]-eig[i-1]
    epsilon[i]=delta[i]-delta[i-1]
  }
  data=matrix(0,nrow=n,ncol=3)
  data=cbind(valeurs_propres=eig, delta=delta, epsilon=epsilon)
  return(data)
}

select_species=function(data,groupes_cah){
  nb.classes=length(levels(as.factor(groupes_cah)))
  moyennes=numeric(nb.classes)
  for(i in 1:nb.classes){
    namegp=names(which(groupes_cah==i))
    effgp=length(which(groupes_cah==i))
    moyennes[i]=sum(data[namegp],na.rm=TRUE)/effgp
  }
  indice.autre=which(moyennes == min(moyennes,na.rm=TRUE))
  noms=names(which(groupes_cah!=indice.autre))
  return(list(noms,indice.autre))
}

extractTableMainSpecies = function(dat,namesMainSpeciesHAC,paramTotal=95,paramLogevent=100){
  
  print("######## STEP 1 COMBINATION OF MAIN SPECIES FROM THE THREE EXPLORATORY METHODS ########")
  t1 <- Sys.time()
  
  # TOTALE
  
  p=ncol(dat)   # Number of species +1
  print("Calculating proportions...")
  propdat=transformation_proportion(dat[,2:p])
  namesSpecies=colnames(propdat)
  
  # Total quantity caught species by species
  sumcol=rep(as.numeric(NA),p-1) #numeric()
  for(i in 2:p) sumcol[i-1]=sum(dat[,i])
  names(sumcol)=namesSpecies
  
  # Percent of each species in the total catch
  propSp=sumcol/sum(sumcol,na.rm=TRUE)*100
  # Columns number of each species by decreasing order of capture
  numSp=order(propSp,decreasing=TRUE)
  # Percent of each species in the total catch by decreasing order
  propSp=cumsum(propSp[order(propSp,decreasing=TRUE)])
  
  Store(objects())
  gc(reset=TRUE)
  
  # We are taking all species until having at least param1% of total catch
  if (is.null(paramTotal) | !is.numeric(paramTotal)) stop("param1 must be numeric between 0 and 100")
  threshold=paramTotal
  pourcent=which(propSp<=threshold)
  # We are taking the name of selected species
  selSpecies=numSp[1:(length(pourcent)+1)]
  namesSelSpeciesTotal=namesSpecies[selSpecies]
  
  
  
  # LOGEVENT
  
  if (is.null(paramLogevent) | !is.numeric(paramLogevent)) stop("paramLogevent must be numeric between 0 and 100")
  
  threshold=paramLogevent
  # Selection of species making up over param1% of logevent's captures
  namesSelSpeciesLogevent=character()
  for (i in namesSpecies) {
    if (!is.na(any(propdat[,i]>=threshold)) && any(propdat[,i]>=threshold)) namesSelSpeciesLogevent=c(namesSelSpeciesLogevent,i)
  }
  
  # Merge with explospecies
  listSpeciesAll <- sort(unique(c(namesMainSpeciesHAC,namesSelSpeciesTotal,namesSelSpeciesLogevent)))
  listSpeciesAll <- listSpeciesAll[!listSpeciesAll=="MZZ"]
  
  # We are building the table with main species and aggregated other species
  datSpecies=building_tab_pca(propdat,listSpeciesAll)
  rownames(datSpecies)=dat[,1]
  
  
  print(" --- end of step 1 ---")
  print(Sys.time()-t1)
  
  return(datSpecies)
  
}


building_tab_pca=function(data,especes){
  p=ncol(data)
  noms=colnames(data)
  ind_princ=which(is.element(noms,especes))
  princ=data[,ind_princ]
  return(princ)
}

withinVar <- function(oneRowOfCluster,centerOfGravityClusti){
  comb   <- rbind(centerOfGravityClusti, oneRowOfCluster)
  sqrComb <- dist(comb)^2
  return(sqrComb)}


test.values=function(groupes,data){
  
  n=nrow(data)
  p=ncol(data)
  noms_var=colnames(data)
  nb_groupes=length(levels(as.factor(groupes)))
  noms_groupes=character(nb_groupes)
  
  stats_globales=matrix(0,nrow=p,ncol=2)
  row.names(stats_globales)=noms_var
  colnames(stats_globales)=c("mean","variance")
  for (i in 1:p){
    stats_globales[i,1]=mean(data[,noms_var[i]])
    stats_globales[i,2]=var(data[,noms_var[i]])
  }
  
  res=matrix(0,nrow=p,ncol=nb_groupes)
  row.names(res)=noms_var
  
  for (j in 1:nb_groupes){
    groupe=which(groupes==j)
    n_k=length(groupe)
    
    for (i in 1:p){
      mu_k=mean(data[groupe,noms_var[i]])
      mu=stats_globales[noms_var[i],"mean"]
      V=stats_globales[noms_var[i],"variance"]
      V_mu_k=(n-n_k)*V/(n_k*(n-1))
      
      if(V_mu_k==0){
        Valeur_test=0
      }else{
        Valeur_test=(mu_k-mu)/sqrt(V_mu_k)
      }
      
      res[i,j]=Valeur_test
      rm(Valeur_test)
    }
    rm(groupe)
    noms_groupes[j]=paste("Cluster",j,sep=" ")
  }
  colnames(res)=noms_groupes
  return(res)
}


targetspecies=function(resval){
  p=nrow(resval)
  nbgp=ncol(resval)
  
  tabnumespcib=data.frame()
  tabnomespcib=data.frame()
  
  for(i in 1:nbgp){
    # qnorm(0.975,mean=0,sd=1)=1.96     (P(resval>1.96)=0.025)
    numespcib=which(resval[,i]>1.96)
    numespcibdec=numespcib[order(resval[numespcib,i],decreasing=TRUE)]
    nomespcib=names(numespcibdec)
    
    nbespgpcib=length(numespcib)
    
    if(nbespgpcib>0){
      for (j in 1:nbespgpcib){
        tabnumespcib[i,j]=numespcibdec[j]
        tabnomespcib[i,j]=nomespcib[j]
      }
    }else{
      tabnumespcib[i,]=NA
      tabnomespcib[i,]=NA
    }
  }
  tabnumespcib=as.matrix(tabnumespcib)
  tabnomespcib=as.matrix(tabnomespcib)
  return(list(tabnumespcib=tabnumespcib,tabnomespcib=tabnomespcib))
}
