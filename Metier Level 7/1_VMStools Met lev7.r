
rm(list=ls())

# library(vmstools)
# library(ggplot2)
# library(ggmap)

packages <- c("maps","mapproj","mapdata","mapplots","RColorBrewer","randomcoloR","GGally",
              "mapplots","dplyr","tidyr","SDMTools","shiny","amap","SOAR","FactoMineR",
              "ggplot2","rstudioapi","cluster","MASS","mda","class","lattice")


              packages <- c("maps","mapproj","mapdata","mapplots","RColorBrewer","randomcoloR","GGally",
              "mapplots","dplyr","tidyr","shiny","amap","SOAR","FactoMineR",
              "ggplot2","rstudioapi","cluster","MASS","mda","class","lattice")

## Now load or install & load all
package.check <- lapply(
  packages,
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE)
      library(x, character.only = TRUE)
    }
  }
)






Country  <- "DEU"
dir.create(paste0("./",Country,"/"))
load(paste0("../Data/FDI_2014_2016",Country,".RData"))   # this object is the object

mainpath <- getwd()
subpath  <- paste0(getwd(),"/",Country,"/")

######################################################################################################################
# select the relevant metiers for the analyses
######################################################################################################################
# modify metier names so that they are compatible with windows paths (i.e. no > or < symbols)
dats$fishery <- gsub(">" , "sup" , dats$fishery)
dats$fishery <- gsub("<" , "inf" , dats$fishery)
dats$FT_REF     <- with(dats , paste0("Y",year,"Q",quarter,"Rect",ICES.rectangle))  
  
                  

#  remove the small metiers based on total landings or value per metier 
#  LAN_SUM   <-apply(dats[,grep("LAN",names(dats))] , 1 , function(x) sum(x,na.rm = T))
metier_tot <- aggregate(landings~fishery,dats, sum)
metier_tot$LAN_PERC <-  with(metier_tot , round(100*landings / sum(landings),1))
metier_tot <- metier_tot[order(metier_tot$landings, decreasing =T) ,]
metier_tot$met_sel <- with(metier_tot , cumsum(landings) < 0.95 * sum(landings))           #  criteria chosen : metiers that together make up 95% of the total value
metier_tot$Met<-as.character(metier_tot$fishery)
metier_tot$Met[!metier_tot$met_sel]  <-"other"
metier_tot  <- aggregate(cbind(landings,LAN_PERC,met_sel)~Met,metier_tot,sum)
  
g<- ggplot(metier_tot , aes(Met , LAN_PERC,fill=factor(met_sel))) + 
                  geom_bar(stat="identity")  + 
                  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
                  scale_fill_discrete(guide=FALSE) +
                  ylab("proportion of sampled landings")
png(file=paste0("./",Country,"/main metiers lev 6.png"))
  print(g)
dev.off()
       


# choose to remove the small metiers or to keep them
keep_all_metiers   <- T
metier_sel         <- metier_tot$Met[metier_tot$met_sel == as.numeric(!keep_all_metiers)]
metier_sel <-unique(dats$fishery)
        
######################################################################################################################
# define the met 7 for each met 5
######################################################################################################################

# function to combine the creation of additional columns and the formatEflalo function
FDI_to_Eflalo <- function(x){
  # need to reshape the data to have a format similar to EFLALO files
  eflalo            <- x  
  eflalo$FAO        <- x$species
  eflalo$LAN        <- x$landings
  
  eflalo$LE_CDATIM  <- NA
  eflalo$VE_REF     <- NA
  eflalo$VE_ID      <- NA
  eflalo$VE_FLT     <- eflalo$LE_MET <- x$fishery
  eflalo$VE_COU     <- Country
  eflalo$FT_DCOU    <- NA
  eflalo$FT_DHAR    <- NA
  eflalo$FT_DDAT    <- NA
  eflalo$FT_DTIME   <- NA
  eflalo$FT_LCOU    <- NA
  eflalo$FT_LHAR    <- NA
  eflalo$FT_LDAT    <- NA
  eflalo$FT_LTIME   <- NA
  eflalo$VE_LEN     <- x$vessel.length
  eflalo$VE_KW      <- NA
  eflalo$VE_TON     <- NA
  eflalo$LE_ID      <- eflalo$FT_REF
  eflalo$LE_GEAR    <- x$gear
  eflalo$LE_MSZ     <- x$mesh.size.range
  eflalo$LE_RECT    <- x$ICES.rectangle
  eflalo$LE_DIV     <- x$area
  eflalo$LE_SUBDIV  <- x$area
  eflalo$LE_CDAT    <- NA
  
  varsel            <- c("FT_REF","LE_CDATIM","VE_REF","VE_ID","VE_FLT","VE_COU","VE_LEN",
                         "VE_KW", "FT_DCOU" , "FT_DHAR","FT_DDAT","FT_DTIME",
                         "FT_LCOU","FT_LHAR" , "FT_LDAT","FT_LTIME" ,
                         "VE_TON","LE_ID","LE_CDAT","LE_GEAR","LE_MSZ","LE_RECT","LE_DIV","LE_SUBDIV",
                         "LE_MET","FAO","LAN")
  eflalo            <- eflalo[,varsel]
  eflalo            <- unique(eflalo)
  eflalo$FAO        <- paste0("LE_KG_",eflalo$FAO)  
      
  #eflalo            <- aggregate(LAN ~.,data =  eflalo,sum)
  # what out here some contries have different values for  "specon" and
  # when this column gets removes, you end up with multiple LAN for the same other
  # variables, which produces an error  with the spread function
        
  eflalo            <- tidyr::spread(eflalo , key = FAO , value = LAN , fill=0)
  
  eflalo$VE_REF     <- as.character(eflalo$VE_REF)
  eflalo$VE_FLT     <- as.character(eflalo$VE_FLT)
  eflalo$VE_COU     <- as.character(eflalo$VE_COU)
  eflalo$VE_LEN     <- as.numeric(as.character(eflalo$VE_LEN))
  eflalo$VE_KW      <- as.numeric(as.character(eflalo$VE_KW))
  if("VE_TON" %in% colnames(eflalo)) eflalo$VE_TON <- as.numeric(as.character(eflalo$VE_TON))
  eflalo$FT_REF     <- as.character(eflalo$FT_REF)
  eflalo$FT_DCOU    <- as.character(eflalo$FT_DCOU)
  eflalo$FT_DHAR    <- as.character(eflalo$FT_DHAR)
  eflalo$FT_DDAT    <- as.character(eflalo$FT_DDAT)
  eflalo$FT_DTIME   <- as.character(eflalo$FT_DTIME)
  eflalo$FT_LCOU    <- as.character(eflalo$FT_LCOU)
  eflalo$FT_LHAR    <- as.character(eflalo$FT_LHAR)
  eflalo$FT_LDAT    <- as.character(eflalo$FT_LDAT)
  eflalo$FT_LTIME   <- as.character(eflalo$FT_LTIME)
  eflalo$LE_ID      <- as.character(eflalo$LE_ID)
  eflalo$LE_CDAT    <- as.character(eflalo$LE_CDAT)
  if("LE_UNIT"  %in% colnames(eflalo)) eflalo$LE_UNIT   <- as.character(eflalo$LE_UNIT)
  if("LE_STIME" %in% colnames(eflalo)) eflalo$LE_STIME  <- as.character(eflalo$LE_STIME)
  if("LE_ETIME" %in% colnames(eflalo)) eflalo$LE_ETIME  <- as.character(eflalo$LE_ETIME)
  if("LE_SLAT"  %in% colnames(eflalo)) eflalo$LE_SLAT   <- as.numeric(as.character(eflalo$LE_SLAT))
  if("LE_SLON"  %in% colnames(eflalo)) eflalo$LE_SLON   <- as.numeric(as.character(eflalo$LE_SLON))
  if("LE_ELAT"  %in% colnames(eflalo)) eflalo$LE_ELAT   <- as.numeric(as.character(eflalo$LE_ELAT))
  if("LE_ELON"  %in% colnames(eflalo)) eflalo$LE_ELON   <- as.numeric(as.character(eflalo$LE_ELON))
  eflalo$LE_GEAR    <- as.character(eflalo$LE_GEAR)
  eflalo$LE_MSZ     <- as.numeric(as.character(eflalo$LE_MSZ))
  eflalo$LE_RECT    <- as.character(eflalo$LE_RECT)
  eflalo$LE_DIV     <- as.character(eflalo$LE_DIV)
  if("LE_MET" %in% colnames(eflalo)) eflalo$LE_MET <- as.character(eflalo$LE_MET)
  for(i in c(grep("_KG_",colnames(eflalo)),grep("_EURO_",colnames(eflalo)))) eflalo[,i] <- as.numeric(as.character(eflalo[,i]))
  return(eflalo)
}

eflalo <- FDI_to_Eflalo(dats)


eflalo <- unique(eflalo)

dim(eflalo[,1:25])
dim(unique(eflalo[,1:25]))

           
# now run the analysis

# source sripts and data
source("./Common Scripts/getEflaloMetierLevel7_funV2.r")
source("./Common Scripts/getTableAfterPCA_fun.r")
source('./Common Scripts/getMetierClusters_fun.r')
load(file = paste0(strsplit(subpath, "Metier Level 7")[[1]][1],"Metier Level 7/Common Scripts/correspLevel7to5.rda"))
load(file = paste0(strsplit(subpath, "Metier Level 7")[[1]][1],"Metier Level 7/Common Scripts/correspMixedMetier.rda"))


metier_sel <- as.list (metier_sel)

eflalo_met7  <- lapply (metier_sel , function(x)  
              {   #     x<- metier_sel[[7]]
                # x <- "Balt PEL_TRAWL 32-54"
                
              setwd(paste0(mainpath,"/",Country,"/"))
              # create final object ot return in case the classification failed (for lack of data)
              eflalo_metiers_level7 <- "Analysis failed"
              
              cat("running Metier level 7 definition for",x,"\n" )
              analysisName=paste("metier_analysis_",x,sep="")  
                                                                            
              if (!file.exists(analysisName)) dir.create(analysisName)
              setwd(file.path(subpath,analysisName))
     
              # Format
              eflalo_x=eflalo[eflalo$LE_MET== x,]  # main data set at "eflalo"like format, which is required to run the analysis

              # Return a fully working eflalo dataset with a metier at DCF Level7 for each logbook event
              try(
              {eflalo_metiers_level7=getEflaloMetierLevel7_proby(eflalo_x, analysisName, subpath, critData="KG", runHACinSpeciesSelection=TRUE, 
              paramTotal=95,paramLogevent=100, critPca="PCA_70", 
              algoClust="CLARA" , testlevel=3)}
                     , silent = T)
              setwd(subpath)
                
                   
              try({
              # delete landings and value columns for the few species selected in the analysis, will be replace by the landing data for all species
              eflalo_metiers_level7  <- eflalo_metiers_level7[,-grep("LE_KG",names(eflalo_metiers_level7)) ] # remove landings data 
              # now there is only the "cluster" (met lev7) in the data base, we need to merge with the whole landings and discards data
              eflalo_metiers_level7  <- merge(eflalo_metiers_level7[,c("FT_REF","CLUSTER")],eflalo_x,all.x=T)
              eflalo_metiers_level7$CLUSTER  <- as.character(eflalo_metiers_level7$CLUSTER)
              # now put all species as rows
              eflalo_metiers_level7  <- tidyr::gather(eflalo_metiers_level7, key="SP",value="LE_KG",grep("LE_KG",names(eflalo_metiers_level7)),na.rm=T)  
              },        silent=T)
              
              return(eflalo_metiers_level7)
              }  )
  
 
# to check for how many metier the analysis failed
sublist <- unlist(lapply(eflalo_met7 , function(x) class(x)!="character"))
sublist <- c(1:length(sublist))[sublist]
   
# combine all the metiers
res          <- do.call("rbind", eflalo_met7[sublist])
res$SP       <- gsub("LE_KG_","",res$SP)
res          <- subset(res , LE_KG != 0)
 


# remove unnecessary columns
NAcnt <-  apply(res,2,function(x) sum(is.na(x))) / dim(res)[1]
NAvar <-  names(res)[NAcnt==1]
res   <-  res[,!is.element(names(res),NAvar)]
   
   
   
rm(list=c("eflalo","LE_EURO_SUM","LE_KG_SUM","eflalo_met7"))


# add english name to species code
spname <- subset(correspLevel7to5,is.element(X3A_CODE,unique(res$SP)))[,c("X3A_CODE","English_name")]
names(spname)[1] <-  "SP"
res  <- merge(res,spname,all.x=T)
res$SP  <- with(res , paste0(SP," (",English_name,")"))


res$LE_QUARTER  <- substr(res$FT_REF , start = 6 , stop = 7)
res$LE_YEAR    <-  substr(res$FT_REF , start = 2 , stop = 5)


# get the coordinates of the haul to be able to plot the clusters in space
pos <- dats[,c("FT_REF" , "SI_LATI" , "SI_LONG")]
pos <- unique(pos)
res <- merge( res , pos , all.x = T)

# extract the identified target species from the cluster name
res$target  <-  sub("^[^/]*", "", res$CLUSTER)
res$target  <-  sub("/", "",      res$target)
res$target[res$target == ""] <-   "No target"

save(res,file=paste0(subpath,"/res",Country,"Met Lev7.RData"))




  
  