

packages <- c("maps","mapproj","mapdata","mapplots","RColorBrewer","randomcoloR","rstudioapi",
              "GGally","corrplot","factoextra","mapplots","dplyr","tidyr","SDMTools","shiny")

## Now load or install & load all
package.check <- lapply(
  packages,
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE)
      library(x, character.only = TRUE)
    }
  }
)

# set path
mainpath <- getwd()
setwd(mainpath)

country <- "NLD"


load(paste0("./",country,"/res",country,"Met Lev7.RData"))
# reformat
dats<-res
dats$LE_YEAR <- as.numeric(dats$LE_YEAR)

server <-shinyServer(function(input, output) {

  output$moreControls <- renderUI({ 
      ctry        <-  input$country
      cat(ctry)
      res1        <- subset(dats,  is.element(VE_COU, ctry))
      cat(dim(res1))
      tagList( selectInput("gear_type","Gear",unique(res1$LE_GEAR),multiple=T,selectize = T)  )
      })
  
  output$moreControls2 <- renderUI({ 
    
      gear.type   <-  input$gear_type
      cat(gear.type)
      ctry        <-  input$country
      cat(ctry)
      res1        <- subset(dats, is.element(LE_GEAR, gear.type)  & is.element(VE_COU, ctry))
      cat(dim(res1))
      tagList( selectInput("met5.1","Metier.lev6",unique(res1$LE_MET),multiple=T,selectize = T)  ,
                sliderInput("lat.range", label = "limits of latitude", min = min(res1$SI_LATI,na.rm=T)-2,
                                                                     max = max(res1$SI_LATI,na.rm=T)+2, value = c(min(res1$SI_LATI,na.rm=T)-2,max(res1$SI_LATI,na.rm=T)+2)) ,
                sliderInput("long.range", label = "limits of longitude", min = min(res1$SI_LONG,na.rm=T)-2,
                                                                     max = max(res1$SI_LONG,na.rm=T)+2, value = c(min(res1$SI_LONG,na.rm=T)-2,max(res1$SI_LONG,na.rm=T)+2)) )
      })



  output$mapMet <- renderPlot({

    latrange   <- input$lat.range
    longrange  <- input$long.range
    yr         <-  input$year.1
    met5   <-  input$met5.1
    Q      <-  paste0("Q",as.numeric(input$Q.1))
    gear.type   <-  input$gear_type
    ctry        <-  input$country
    
    
    ##    latrange <- c(48,60) ; longrange <- c(-10,3) ; yr <- 2014:2016 ; met5 <- "TBB_DEF_70-99_0_0" ; Q<- paste0("Q",1:4)  ;  cumlim  <- 90; ctry <- "BEL"
    ##    latrange <- c(48,51) ; longrange <- c(-3,3) ; yr <- 2014:2016 ; met5 <- "NONE" ; Q<- 1:4  ;  cumlim  <- 90 
    
    res1  <- subset(dats ,   is.element( LE_MET , met5) & is.element( LE_YEAR , yr) & is.element(LE_QUARTER , Q) & is.element(VE_COU,ctry))
    cat(dim(res1),"\n")
    res2 <- aggregate( LE_KG ~ target + LE_RECT + SI_LATI + SI_LONG  , res1 , sum)
    res2  <- tidyr::spread(data = res2 , key = target , value  = LE_KG , fill= 0 )
    res2  <- subset(res2 , SI_LATI>= latrange[1]  & SI_LATI<= latrange[2] )
    res2  <- subset(res2 , SI_LONG>= longrange[1] & SI_LONG<= longrange[2] )
    
    cat(dim(res2),"\n")
    cat(latrange,"\n")
    cat(longrange,"\n")
    
    #names(res2)[3]<- "None"
    tot <- apply(res2[,-c(1:3)],2,sum)
    tot <- round(100*tot/sum(tot),1)
    tot <- sort(tot,decreasing  = T)
    cumlim <- input$cumlim
    cat(cumlim)
    tot<-(cumsum(tot) - cumlim)>=0
    tot<-cumsum(tot)
    tot[tot<=1] <-1
    tot[tot!=1]<-0
    tot  <- tot[names(res2[,-c(1:3)])]
    Names<- names(tot)
    Names[tot == 0]  <- ""
    
    if(length(Names[Names != ""]) <2)  stop("increase cumulated percentage of total catches")
    
    resmain  <- res2[,  c("SI_LATI","SI_LONG",names(tot)[tot==1])]
    idx <- apply(resmain[,-c(1,2)] , 1, sum)
    resmain  <- resmain[idx>0,]
    pos <- resmain[,1:2]
    resmain <-  resmain[,-c(1:2)]
    if(dim(resmain)[2] == 0)  stop("increase cumulated percentage of total catches")
    resminor <- res2[,  names(tot)[tot==0]]
    print(resmain)
    print(pos)
    ### plot the main metier 7
    coul  <- rainbow(dim(resmain)[2])
    maxL  <- max(apply(resmain , 1 , sum))
    #   
    
    map(col = "grey80", border = "grey40", fill = TRUE, xlim = c(min(pos[,2])-1,max(pos[,2])+1), ylim =  c(min(pos[,1])-1,max(pos[,1])+1), main = "main metier lev7")
    map(col = "grey80", border = "grey40", fill = TRUE, xlim = longrange, ylim =  latrange, main = "main metier lev7")
    coord2 <- mapproject(pos[,2],pos[,1])#
    for (i in 1:dim(resmain)[1]) add.pie(z=as.matrix(resmain[i,]), x=coord2$x[i], y=coord2$y[i], labels =NA, radius = sum(0.3*resmain[i,])^0.5/maxL^0.5 +0.05 , col= coul)
  }, height="auto")
  
  
output$MetBars <- renderPlot({

    latrange   <- input$lat.range
    longrange  <- input$long.range
    yr         <-  input$year.1
    met5   <-  input$met5.1
    Q      <-  paste0("Q",as.numeric(input$Q.1))
    gear.type   <-  input$gear_type
    ctry        <-  input$country
    
    
    ##    latrange <- c(48,60) ; longrange <- c(-10,3) ; yr <- 2014:2016 ; met5 <- "BEAM 80-99" ; Q<- paste0("Q",1:4)  ;  cumlim  <- 90; ctry <- "NLD"
    ##    latrange <- c(48,51) ; longrange <- c(-3,3) ; yr <- 2014:2016 ; met5 <- "NONE" ; Q<- 1:4  ;  cumlim  <- 90 
    
    res1  <- subset(dats ,   is.element( LE_MET , met5) & is.element( LE_YEAR , yr) & is.element(LE_QUARTER , Q) & is.element(VE_COU,ctry))
    cat(dim(res1),"\n")
    res2 <- aggregate( LE_KG ~ target + LE_RECT + SI_LATI + SI_LONG  , res1 , sum)
    res2  <- tidyr::spread(data = res2 , key = target , value  = LE_KG , fill= 0 )
    res2  <- subset(res2 , SI_LATI>= latrange[1]  & SI_LATI<= latrange[2] )
    res2  <- subset(res2 , SI_LONG>= longrange[1] & SI_LONG<= longrange[2] )
    
    cat(dim(res2),"\n")
    cat(latrange,"\n")
    cat(longrange,"\n")
    
    #names(res2)[3]<- "None"
    tot <- apply(res2[,-c(1:3)],2,sum)
    tot <- round(100*tot/sum(tot),1)
    tot <- sort(tot,decreasing  = T)
    cumlim <- input$cumlim
    cat(cumlim)
    tot<-(cumsum(tot) - cumlim)>=0
    tot<-cumsum(tot)
    tot[tot<=1] <-1
    tot[tot!=1]<-0
    tot  <- tot[names(res2[,-c(1:3)])]
    Names<- names(tot)
    Names[tot == 0]  <- ""
    
    if(length(Names[Names != ""]) <2)  stop("increase cumulated percentage of total catches")
    
    resmain  <- res2[,  c("SI_LATI","SI_LONG",names(tot)[tot==1])]
    idx <- apply(resmain[,-c(1,2)] , 1, sum)
    resmain  <- resmain[idx>0,]
    pos <- resmain[,1:2]
    resmain <-  resmain[,-c(1:2)]
    if(dim(resmain)[2] == 0)  stop("increase cumulated percentage of total catches")
    resminor <- res2[,  names(tot)[tot==0]]
    print(resmain)
    print(pos)
    ### plot the main metier 7
    coul  <- rainbow(dim(resmain)[2])
    resmain <- apply(resmain,2,sum)
    resmain <- data.frame(MetLev7 = names(resmain) , resmain)
    #   
    bp<- ggplot(resmain, aes(x=MetLev7, y=resmain, fill=MetLev7))+
         geom_bar(width = 1, stat = "identity")     + 
         scale_fill_manual("Target Species", values = coul)  +   ylab("tonnes") +
         coord_flip()        +
         theme(axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        legend.position="bottom")   +
         guides(fill=guide_legend(ncol=1))
print(bp)
  
  }) 
  
  
})






# Define UI for application that draws a histogram
ui<-shinyUI(navbarPage("Task 2 : Relavant fisheries",
                   tabPanel("Maps of landing per Target species",
                            fluidPage(
                              titlePanel(h4("Data selection")),
                              sidebarLayout(position = "left",
                                            sidebarPanel(width= 2 ,
                                              selectInput("country","Country", choice = c(sort(unique(as.character(dats$VE_COU)))) ,  multiple=F) ,
                                              uiOutput("moreControls") ,
                                              uiOutput("moreControls2") ,
                                              sliderInput("cumlim", "cumulated percentage of total landings", 50, 100, 95),         
                                              checkboxGroupInput("year.1","Year:", choiceNames=c(sort(unique(as.character(dats$LE_YEAR)))) ,
                                                                            choiceValues=c(sort(unique(as.character(dats$LE_YEAR)))) ,
                                                                            selected =(sort(unique(as.character(dats$LE_YEAR)))),
                                                                            inline = T)  ,
                                              checkboxGroupInput("Q.1",label = "Quarter",choices = list("Q1" = 1, "Q2" = 2, "Q3" = 3, "Q4" = 4),selected = 1:4,inline = T)
                                            ),
                                            mainPanel("",
                          fluidRow(
                           splitLayout(
                            style = "border: 1px solid silver;",
                            cellWidths = c("70%", "30%"), 
                            plotOutput("mapMet"), 
                            plotOutput("MetBars"))

                          )
                )
                                            )
                            ) #end of FluidPage
                   )#end of tabPanel)
) )


# Run app
shinyApp(ui = ui, server = server)
  
  
    